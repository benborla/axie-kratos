export const store = payload => {
  localStorage.setItem('energy', Number(payload))
}

export const get = () => {
  return localStorage.getItem('energy')
}

export const consume = () => {
  localStorage.setItem('energy', Number(get()) - 1)
}
