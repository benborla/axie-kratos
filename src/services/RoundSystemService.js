import { current } from "@reduxjs/toolkit"

const matchRows = []
const matchLogs = []

export const startMatch = () => {}
export const nextRound = (round, zeroEnergy, oneEnergy, twoEnergy, opponentSkipped) => {
  // get the current logs
  matchRows.push({
    round,
    zeroEnergy,
    oneEnergy,
    twoEnergy,
    opponentSkipped
  })

  localStorage.setItem('battleLog', JSON.stringify(matchRows))
}

export const recordMatch = status => {
  switch (status) {
    case 'win':
      localStorage.setItem('wins', getNumberOfWins() + 1)
      break;
    case 'lose':
      localStorage.setItem('losses', getNumberOfLosses() + 1)
      break;
    case 'draw':
      localStorage.setItem('draws', getNumberOfDraws() + 1)
      break;
  }

  clearBattleLog()
}

export const getRounds = () => matchRows

export const getNumberOfWins = () => {
  return Number(localStorage.getItem('wins'))
}

export const getNumberOfLosses = () => {
  return Number(localStorage.getItem('losses'))
}

export const getNumberOfDraws = () => {
  return Number(localStorage.getItem('draws'))
}

export const clearBattleLog = () => {
  localStorage.removeItem('battleLog')
}

export const clearAll = () => {
  clearBattleLog()
  localStorage.removeItem('wins')
  localStorage.removeItem('losses')
  localStorage.removeItem('draws')
}
