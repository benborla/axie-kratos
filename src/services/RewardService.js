export const REWARD_800 = 1
export const REWARD_950 = 3
export const REWARD_1150 = 6
export const REWARD_1350 = 9
export const REWARD_1450 = 12
export const REWARD_1550 = 15
export const REWARD_1650 = 18
export const REWARD_1750 = 20

export const getSlp = mmr => {
  if (between(mmr, 800, 950)) {
    return REWARD_800
  }

  if (between(mmr, 951, 1150)) {
    return REWARD_950
  }

  if (between(mmr, 1151, 1350)) {
    return REWARD_1150
  }

  if (between(mmr, 1351, 1450)) {
    return REWARD_1350
  }

  if (between(mmr, 1451, 1550)) {
    return REWARD_1450
  }

  if (between(mmr, 1551, 1650)) {
    return REWARD_1550
  }

  if (between(mmr, 1651, 1750)) {
    return REWARD_1650
  }

  if (mmr >= 1751) {
    return REWARD_1750
  }
}

function between (mmr, min, max) {
  return mmr >= min && mmr <= max
}
