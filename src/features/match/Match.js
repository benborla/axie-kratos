import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import CardLayout from "../../components/CardLayout";
import CardItem from "../../components/CardLayout/CardItem";
import { setMatchParameters, selectOpponentEnergy, resetState } from "./matchSlice";

export function Match() {
  const dispatch = useDispatch();
  const currentOpponentEnergy = useSelector(selectOpponentEnergy)
  const [currentNumUsedCards, setCurrentNumUsedCards] = useState(0);
  const [currentEnergyGainUsed, setCurrentEnergyGainUsed] = useState(0);
  const [currentEnergyStolen, setCurrentEnergyStolen] = useState(0);
  const [currentRound, setCurrentRound] = useState(1);

  const setNumberOfCardsUsed = () => {
    let countCurrentCards = currentNumUsedCards + 1;
    setCurrentNumUsedCards(countCurrentCards);
  };

  const setEnergyGainUsed = () => {
    let countEnergyGainCards = currentEnergyGainUsed + 1;
    setCurrentEnergyGainUsed(countEnergyGainCards);
  };

  const setEnergyStolen = () => {
    let countEnergyStolen = currentEnergyStolen + 1;
    setCurrentEnergyStolen(countEnergyStolen);
  };

  const calculateEnergy = () => {
    let countCurrentRound = currentRound + 1;
    setCurrentRound(countCurrentRound);
    dispatch(
      setMatchParameters({
        currentRound,
        currentNumUsedCards,
        currentEnergyGainUsed,
        currentEnergyStolen,
      })
    );
  };

  const localResetState = () => {
    setCurrentNumUsedCards(0)
    setCurrentEnergyGainUsed(0)
    setCurrentEnergyStolen(0)
    setCurrentRound(1)
    dispatch(resetState())
  };

  const __render = () => {
    return (
      <>
        
        <small><em>If you like this project you may send a small token to these address:</em></small><br/>
        <small>Ronin Address: ronin:d5867b141470759dac5a8f3b1f1c5624ec335f4e</small><br/>
        <small>ETH Address: 0x1E16588A642fE931Cd1c3089aEe1041739f7F4A9</small><br/>
        <div className="divider">ROUND</div>
        <h2 className="mb-5 text-3xl font-extrabold">
          <button className="btn btn-circle btn-lg no-animation">{currentRound}</button>
        </h2>
        <CardLayout>
          <CardItem
            label="Opponent's ⚡"
            value={currentOpponentEnergy}
            tips="This indicates the current energy your opponent has."
            type="secondary"
          />

          <CardItem
            label="# of Card Used"
            value={currentNumUsedCards}
            tips="Press the number of cards that's been used in this round, except 0
              energy cards."
            event={() => setNumberOfCardsUsed()}
          />

          <CardItem
            label="⚡ Gain Used"
            value={currentEnergyGainUsed}
            tips="Enter the number of times the oppoents gained an energy from this round."
            type="accent"
            event={() => setEnergyGainUsed()}
          />

          <CardItem
            label="Energy I Stole"
            value={currentEnergyStolen}
            tips="Press the amount of energies you stole from your opponent."
            type="secondary"
            event={() => setEnergyStolen()}
          />
        </CardLayout>
        <div className="flex flex-wrap items-start space-x-2 flex-row mt-5 content-center">
          <button
            className="btn btn-wide btn-lg btn-block"
            onClick={() => calculateEnergy()}
          >
            Next round
          </button>
        </div>
        <div className="divider">ACTION</div>

        <div className="flex flex-wrap items-start space-x-2 flex-row mt-5 content-center">
          <button
            className="btn btn-wide btn-lg btn-block btn-primary"
            onClick={() => localResetState()}
          >
            New Match
          </button>
        </div>
        <br />
        <small>If you hav a feedback or feature request, you may send an email to benborla@icloud.com</small>
      </>
    );
  };

  return __render();
}
