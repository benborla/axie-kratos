import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

const initialState = {
  opponentsEnergy: 3
}

export const matchSlice = createSlice({
  name: 'match',
  initialState,
  reducers: {
    setMatchParameters: (state, action) => {
      const round = Number(action.payload.currentRound)
      const cardsUsed = Number(action.payload.currentNumUsedCards)
      const energyGain = Number(action.payload.currentEnergyGainUsed)
      const energyStolen = Number(action.payload.currentEnergyStolen)
      const energyPerRound = 2
      const energyOnFirstRound = 3
      let currentEnergy = state.opponentsEnergy || 2

      // first round
      if (round < 2) {
        currentEnergy = ((energyOnFirstRound - (cardsUsed + energyStolen)) + energyGain) + energyPerRound
      } else {
        currentEnergy = (currentEnergy - (cardsUsed + energyStolen)) + energyGain + energyPerRound
      }

      state.opponentsEnergy = currentEnergy

    },
    resetState: state => initialState
  },
})

export const { setMatchParameters, resetState } = matchSlice.actions
export const selectOpponentEnergy = state => state.match.opponentsEnergy

export default matchSlice.reducer
