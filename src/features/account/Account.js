import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  accountData,
  getAccountData,
  setRoninId,
  errorMessage,
  roninId,
  status
} from './accountSlice'
import {
  store as storeEnergy,
  get as getEnergy,
  consume as consumeEnergy
} from '../../services/EnergyService'
import {
  getSlp
} from '../../services/RewardService'
import { Match } from '../match/Match'

export function Account () {
  const data = useSelector(accountData)
  const error = useSelector(errorMessage)
  const dispatch = useDispatch()
  const requestStatus = useSelector(status)
  const [user, setUser] = useState({})
  const [roninInput, setRoninInput] = useState('')
  const [energyInput, setEnergyInput] = useState(getEnergy() || 0)
  const [cachedRonin, setCachedRonin] = useState(localStorage.getItem('ronin'))

  useEffect(() => {
    if (cachedRonin !== '') {
      dispatch(setRoninId(cachedRonin))
    }

    if (cachedRonin && isValidRoninId(cachedRonin) && error === '') {
      dispatch(getAccountData(cachedRonin))
    }
  }, [cachedRonin])

  useEffect(() => {
    userInfo()
  })

  const userInfo = async () => {
    const info = await data
    if (info && requestStatus === 'idle') {
      setUser(info)
    }
  }

  const addRoninId = payload => {
    const inputRonin = ''
    dispatch(setRoninId(payload))
    localStorage.setItem('ronin', payload)
    setCachedRonin(payload)
  }

  const clearCache = () => {
    // remove invalid ronin in cache
    localStorage.removeItem('ronin')
    localStorage.removeItem('energy')
    setCachedRonin('')
    setRoninInput('')
  }

  const validateRoninId = payload => {
    if (isValidRoninId(payload)) {
      addRoninId(payload)
    } else {
      clearCache()
      alert('Invalid Ronin ID, Ronin ID should start with the following format ronin:d3123...')
    }
  }

  const isValidRoninId = payload => {
    if (!payload.includes('ronin:')) {
      return false
    }

    return true
  }

  const __renderLoading = () => {
    return <h1>Fetching...</h1>
  }

  const __renderError = () => {
    return <h1>Unable to read user data</h1>
  }

  const __renderSetup = () => {
    return (
      <div>
        <input
          type='text'
          name='ronin_id'
          placeholder='Set Ronin ID'
          value={roninInput}
          onChange={(e) => setRoninInput(e.target.value)}
        />
        <button
          onClick={() => validateRoninId(roninInput)}
        >Set
        </button>
      </div>
    )
  }

  const __render = () => {
    const ign = user && user.ign
    const mmr = user && user.mmr

    return (
      <div>
        <h1>{ign}</h1>
        <h2>{mmr}</h2>
        <h2>Estimated SLP per win {getSlp(mmr)}</h2>
        <small>Note: SLP reward on arena will based also on your opponent's MMR</small>
        <div>
          <input
            type='number'
            name='energy_count'
            placeholder='Set energy'
            value={energyInput}
            onChange={(e) => setEnergyInput(e.target.value)}
          />
          <button
            onClick={() => storeEnergy(energyInput)}
          >Set Energy
          </button>
        </div>
        <div>
          <button
            onClick={() => clearCache()}
          >Logout
          </button>
        </div>

        <Match />
      </div>
    )
  }

  if (cachedRonin === null || isValidRoninId(cachedRonin) === false) {
    return __renderSetup()
  }

  if (requestStatus === 'loading') {
    return __renderLoading()
  }

  if (requestStatus === 'error') {
    return __renderError()
  }

  // default render
  return __render()
}
