import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { fetchAccount } from './accountAPI'

const initialState = {
  roninId: '',
  data: {},
  error: '',
  status: 'idle'
}

export const getAccountData = createAsyncThunk(
  'account/get',
  async (roninAddress) => {
    const response = await fetchAccount(roninAddress)

    return response.data
  }
)

export const accountSlice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    setRoninId: (state, action) => {
      state.roninId = action.payload
    }
  },
  extraReducers: builder => {
    builder
      .addCase(getAccountData.pending, state => {
        state.status = 'loading'
      })
      .addCase(getAccountData.fulfilled, (state, action) => {
        state.status = 'idle'
        state.data = action.payload
      })
      .addCase(getAccountData.rejected, (state, action) => {
        state.status = 'error'
        state.data = action.payload
      })
  }
})

export const { setRoninId } = accountSlice.actions
export const accountData = state => state.account.data
export const errorMessage = state => state.account.error
export const roninId = state => state.account.roninId
export const status = state => state.account.status
export default accountSlice.reducer
