import axios from 'axios'

export function fetchAccount (roninAddress) {
  // check if the ronin address has ronin:
  // if it has, then replace it with 0x
  const cleanedRonin = roninAddress.replace('ronin:', '0x')
  const url = `https://api.lunaciarover.com/stats/${cleanedRonin}`

  return new axios.get(url)
}
