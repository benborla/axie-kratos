import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'
import matchReducer from '../features/match/matchSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
		match: matchReducer
  }
})
