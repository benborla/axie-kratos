import React from "react";
import "./App.css";
import { Match } from './features/match/Match'

function App() {
  return (
    <div className="App">
      <div className="hero min-h-screen md:min-h-full">
        <div className="text-center hero-content">
          <div className="max-w-md">
            <h1 className="mb-5 text-3xl font-semibold">Arena Energy Counter</h1>
            <p className="mb-5"></p>
            <Match />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
