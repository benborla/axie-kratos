import React from "react";

const CardLayout = React.memo(({ children }) => (
  <div className="pt-2">
    <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-2 gap-6">
      {children}
    </div>
  </div>
));

export default CardLayout;
