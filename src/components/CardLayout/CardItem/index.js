import React from "react";

const CardItem = ({label, value, tips, type, event}) => {

  const cardType = type || 'primary'

  return <div className={`card shadow-2xl lg:card-side bg-${cardType} text-primary-content`} onClick={event}>
    <div className="card-body text-center">
      <div className="card-title">
        {label}
        <div className="badge badge-lg mx-2 badge-accent font-medium">{value}</div>
      </div>
      <div className="alert">
        <label className="text-xs">
            {tips}
        </label>
      </div>
    </div>
  </div>
}

export default CardItem;
